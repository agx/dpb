## Contents

* [DPB-Book](#dpb-book)
* [Scripts](#scripts)
* [Dependencies](#dependencies)
* [Translators](#translators)
* [License](#license)

# DPB-Book

The book is recently available in German. It is written in NoWeb and 
contains the script.

The script create-book.sh generates the book in the formats pdf and epub. 

The book itself is published as PDF and EPUB under 
https://people.debian.org/~mechtilde/Dokumentation/.

# Scripts

All scripts are available with a user interface in English. The main script
can help to build Debian packages using git-buildpackage.

With the script create-buildscript.sh the main script build-gbp.sh and 
other scripts like plugins can be generated.

# Dependencies

To extract the program script and the book in pdf and epub format you
have to install the following packages:

For the script:
* noweb

and additional for creating the book

* texlive
* texlive-latex-extra
* texlive-lang-german
* texlive-lang-japanese

To use the scripts you have to install the dependencies listed in the 
headers of the scripts.

# Translators

To get started:

First you need to install:

    sudo apt install gtranslator noweb po4a texlive-lang-french texlive-lang-german texlive-lang-japanese translate-shell tidy

Then you have to create a working directory:

    git clone https://salsa.debian.org/ddp-team/dpb
    cd dpb

Checkout your language branch if exists:
 
    git checkout translation/<ISO-Code>

and start the translation using the \*.po-files.

If the language directory doesn't exists

Start the script:

    ./create-po4a.sh 
    
and choose task 3 to create it. You can create an own branch for your 
translation with

    git checkout -b translation/<ISO-Code>

To create the book use the script and choose task 1 and then task 5.

# License

The book has the following license:

Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen
4.0 International Lizenz (CC BY-SA 4.0)
https://creativecommons.org/licenses/by-sa/4.0/legalcode

The code is licensed under GNU General Public License Version 3 or
(at your option) any later version
