#!/bin/bash
noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex # contains preamble
noweave -index -delay Title.nw > GBP-Title.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
noweave -index -delay Part1.nw > GBP-Part1.tex
noweave -index -delay Part2.nw > GBP-Part2.tex
noweave -index -delay Part3.nw > GBP-Part3.tex
noweave -index -delay Part4.nw > GBP-Part4.tex
noweave -index -delay Part5.nw > GBP-Part5.tex
noweave -index -delay Part6.nw > GBP-Part6.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -index -delay BuildWithGBP.nw > BuildWithGBP.tex
#noweave -filter l2h -index -html BuildWithGBP.nw | htmltoc > BuildWithGBP.html
makeindex BuildWithGBP.idx

for ((i=4; i>0; i--))
do
echo $i
pdflatex BuildWithGBP.tex
# Literaturverzeichnis erstellen
bibtex BuildWithGBP
done
# tex4ebook -f mobi BuildWithGBP.tex
tex4ebook -f epub BuildWithGBP.tex ../

# Remove auxillary *.html files which are needed for epub
rm *.html
